extern crate blake3;
use rayon::prelude::*;
use std::collections::{HashMap, HashSet};
use std::path::PathBuf;
use structopt::clap::arg_enum;
use structopt::StructOpt;
use walkdir::WalkDir;

/// Hash a file, return the hash
fn hash_file(filename: &PathBuf) -> blake3::Hash {
    let mut hasher = blake3::Hasher::new();
    hasher.update(&std::fs::read(filename).unwrap());
    hasher.finalize()
}

/// Walk a list of directories, return a list of files
fn get_all_files(paths: Vec<PathBuf>) -> Vec<PathBuf> {
    let mut files = Vec::new();
    for path in paths.into_iter() {
        for entry in WalkDir::new(path)
            .into_iter()
            .filter_map(|e| e.ok())
            .filter(|e| e.path().is_file())
        {
            files.push(entry.into_path());
        }
    }
    files
}

/// Hash every file in a list of files, parallelized, return a hash-map of hash -> path
fn hash_all_files(paths: Vec<PathBuf>) -> HashMap<blake3::Hash, PathBuf> {
    paths.into_par_iter().map(|p| (hash_file(&p), p)).collect()
}

arg_enum! {
    #[derive(Debug)]
    enum CompMode {
        Intersect,
        Complement,
    }
}

/// Check the hashes of all files in some dirs against files in other dirs.
#[derive(Debug, StructOpt)]
#[structopt(
    name = "hashchecker",
    about = "Check all files in some dirs against all files in other dirs."
)]
struct Cli {
    /// Activate debug mode
    #[structopt(short, long)]
    verbose: bool,

    /// Choose how the results are compared
    #[structopt(short, long, possible_values = &CompMode::variants(), case_insensitive=true, default_value="Complement")]
    mode: CompMode,

    /// paths whichs files are checked against the check paths
    #[structopt(long = "in", short, parse(from_os_str))]
    in_paths: Vec<std::path::PathBuf>,

    /// paths to check against
    #[structopt(long = "check", short, parse(from_os_str))]
    check_paths: Vec<std::path::PathBuf>,
}

fn main() {
    let opt = Cli::from_args();
    let in_files_map = hash_all_files(get_all_files(opt.in_paths));
    let check_files_map = hash_all_files(get_all_files(opt.check_paths));
    let in_files_set: HashSet<blake3::Hash> = in_files_map.keys().cloned().collect();
    let check_files_set: HashSet<blake3::Hash> = check_files_map.keys().cloned().collect();
    if opt.verbose {
        println!("In File: {:#?}", in_files_map);
        println!("Check Files: {:#?}", check_files_map);
    }
    let result : HashSet<_> = match opt.mode {
        CompMode::Complement => in_files_set.difference(&check_files_set).collect(),
        CompMode::Intersect => in_files_set.intersection(&check_files_set).collect(),
    };
    result.into_iter().for_each(|h| {
        println!(
            "{}",
            in_files_map
                .get(h)
                .expect("Something went very wrong")
                .to_str()
                .unwrap()
        )
    });
}
