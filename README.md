# hashchecker

Simple tool that checks all files in a list of input directories against all files in a
list of check directories.

Prints the list of all input files that are not in one of the check directories.

I use it to see which files I still need to import from a camera SD card.

Uses the blake3 hash function.



## usage

```
hashchecker 0.1.0
Check all files in some dirs against all files in other dirs.

USAGE:
    hashchecker [FLAGS] [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Activate debug mode

OPTIONS:
    -c, --check <check-paths>...    paths to check against
    -i, --in <in-paths>...          paths whichs files are checked against the check paths
```

e.g.

```
hashchecker -i some/dir or/some/other/dir -c /against/this/dir
```

## todo

Profile for more performance, but I assume it's bound by drive speed as-is.
